import Vue from 'vue'
import Vuex from 'vuex'
import WebClient from './WebClient'
import Axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    status: 1,
    discount: 0,
    reg: false,
    orderid: 0,
    token: '',
    username: '',
    role: '',
    orderInfo:[
    ],
    orderItems:[
      { },
    ],
    userList:[
      { },
    ],
    cart: [
      
    ],
    order:[
      { },
    ],
  },
  mutations: {
    setStatus(state, value){
      state.status = value;
    },

    pushCart(state, item) {
      const orderItem = state.cart.find(oi => oi.itemid == item.itemid);
      if (!orderItem) 
      {
        state.cart.push(item);
      } else {
        orderItem.itemCount+=item.itemCount;
        orderItem.itemDiscount += ((item.price * item.itemCount) - ((item.price * item.itemCount)*(state.discount/100)));
      }
    },

    editCart(state, item) {
      const orderItem = state.cart.find(oi => oi.itemid == item.itemid);
      orderItem.itemCount = item.itemCount;
      if (item.itemCount == 0)
      {
        state.cart = state.cart.filter(oi => oi.id != item.id);
      }
    },

    deleteCart(state, item){
      state.cart = state.cart.filter(oi => oi.id != item.id);
    },

    setCart(state, value){
      state.cart = value;
    },

    setUserToken(state, {access_token, userName}){
      state.token = access_token,
      state.username = userName
    },

    setOrderItems(state, data){
      state.orderItems = data 
    },

    setOrderId(state, value){
      state.orderid = value
    },

    setDiscount(state, value){
      state.discount = value
    },

    setOrderInfo(state, data){
      state.orderInfo = data 
    },

    setUserList(state, data){
      state.userList = data 
    },

    setOrders(state, data){
      state.order = data 
    },

    setRole(state, value){
      state.role = value
    },

    setReg(state, value){
      state.reg = value
    }
  },
  actions: {

    logoutUser(context){
      context.commit('setUserToken', {
        access_token: '' , 
        userName: ''});
      context.commit('setRole', '');
    },

    async userReg(context, {Email, Password, Address, Role})
    {
      return WebClient.post('api/Account/Register', {Email, Password, Address, Role})
        .then(function (response) {
          context.commit('setStatus', response.status || response.response.status);
        })
        .catch(function (error) {
            context.commit('setStatus', error.status || error.response.status);
        });
    },

    async userAdd(context, {Email, Password, Address, Role, Discount})
    {
      return WebClient.post('api/Account/Register', {Email, Password, Address, Role, Discount})
        .then(function (response) {
          context.commit('setStatus', response.status || response.response.status);
        })
        .catch(function (error) {
            context.commit('setStatus', error.status || error.response.status);
        });
    },

    async addOrderItem(context, {name, category, price})
    {
      return WebClient.post('api/OrderItems/', {name, category, price}, {headers: {Authorization: 'Bearer ' + context.getters.getToken}})
        .then(function (response) {
          context.commit('setStatus', response.status || response.response.status);
        })
        .catch(function (error) {
            context.commit('setStatus', error.status || error.response.status);
        });
    },

    async deleteOrderItem(context, id)
    {
        return await WebClient.delete('api/OrderItems/' + id, {headers: {Authorization: 'Bearer ' + context.getters.getToken} })
    },
    
    async deleteOrder(context, id)
    {
        return await WebClient.delete('api/Orders/' + id, {headers: {Authorization: 'Bearer ' + context.getters.getToken} })
    },

    async deleteUser(context, userId)
    {
        return await WebClient.delete('api/Account/DeleteUser', {headers: {Authorization: 'Bearer ' + context.getters.getToken}, data: {userId: userId}})
    },

    async editOrderItem(context, {id, name, category, price})
    {
        return await WebClient.put('api/OrderItems/'+ id, {name, category, price}, {headers: {Authorization: 'Bearer ' + context.getters.getToken}})
    },

    async editUser(context, {id, email, address, role, discount})
    {
        return await WebClient.put('api/Account/EditUser', {id, email, address, role, discount}, {headers: {Authorization: 'Bearer ' + context.getters.getToken}})
    },

    async editOrder(context, {id, shipmentDate, status})
    {
        return await WebClient.put('api/Orders/'+ id, {shipmentDate, status}, {headers: {Authorization: 'Bearer ' + context.getters.getToken}})
    },

    async userAuth(context, {username, password})
    {
      const requestBody = 
      'grant_type=password' +
      '&username=' + username + 
      '&password=' + password;
      
      const config = {
        headers: {
          'Content-Type':'application/x-www-form-urlencoded',
        }
      }

      return Axios.post ('https://localhost:44343/Token', requestBody, config)
        .then(function (response) {
          context.commit('setUserToken', {
            access_token: response.data.access_token , 
            userName: response.data.userName});
            WebClient.get('api/Account/UserInfo', {}, {headers: {Authorization: 'Bearer ' + response.data.access_token} })
            .then(function (response) {
              context.commit('setRole', response.data.Role);
              context.commit('setDiscount', response.data.Discount);
            })
            .catch(function (error) {
              context.commit('setStatus', error.response.status);
            });
            
        })
        .catch(function (error) {
          context.commit('setStatus', error.response.status);
        });
    },

    async orderItemList(context)
    {
      return WebClient.get('api/OrderItems')
      .then(function(response){
        context.commit('setOrderItems', response.data);
        context.commit('setStatus', response.status);
      })
      .catch(function (error) {
        context.commit('setStatus', error.response.status);
      });
    },

    async orderCreate(context)
    {
      return WebClient.post('api/Orders', {status: 'Новый'}, {headers: {Authorization: 'Bearer ' + context.getters.getToken} })
      .then(function(response){
        console.log(response);
        context.commit('setOrderId', response.data.id);
      });
    },
    
    async orderCartCreate(context, {Orderitem_id,  ItemCount})
    {
      return WebClient.post('api/CartOrders', {Order_id: context.getters.getOrderId, Orderitem_id ,ItemCount}, {headers: {Authorization: 'Bearer ' + context.getters.getToken} })
    },
    
    async orderList(context)
    {
      return WebClient.get('api/Orders')
      .then(function(response){
        context.commit('setOrders', response.data);
        context.commit('setStatus', response.status);
      })
      .catch(function (error) {
        context.commit('setStatus', error.response.status);
      });
    },
    
    async orderListUser(context)
    {
      return WebClient.get('api/Orders/UserOrders', {}, {headers: {Authorization: 'Bearer ' + context.getters.getToken} })
      .then(function(response){
        context.commit('setOrders', response.data);
        context.commit('setStatus', response.status);
      })
      .catch(function (error) {
        context.commit('setStatus', error.response.status);
      });
    },

    async orderInfo(context, id)
    {
      return WebClient.get('api/Orders/' + id)
      .then(function(response){
        context.commit('setOrderInfo', response.data);
        context.commit('setStatus', response.status);
      })
      .catch(function (error) {
        context.commit('setStatus', error.response.status);
      });
    },

    async userList(context)
    {
      return WebClient.get('api/Account/UserList', {}, {headers: {Authorization: 'Bearer ' + context.getters.getToken} })
      .then(function(response){
        context.commit('setUserList', response.data);
        context.commit('setStatus', response.status);
      })
      .catch(function (error) {
        context.commit('setStatus', error.response.status);
      });
    }
  },
  getters: {
    getStatus: (state) => {
      return state.status;
    },
    getUsername: (state) => {
      return state.username;
    },
    getOrderItems: (state) => {
      return state.orderItems  || [];
    },
    getOrders: (state) => {
      return state.order || [];
    },
    getOrderInfo: (state) => {
      return state.orderInfo;
    },
    getUserList: (state) => {
      return state.userList || [];
    },
    getRole: (state) => {
      return state.role;
    },
    getReg: (state) => {
      return state.reg;
    },
    getToken: (state) => {
      return state.token
    },
    getCart: (state) => {
      return state.cart
    },
    getDiscount: (state) => {
      return state.discount
    },
    getOrderId: (state) => {
      return state.orderid
    }
  },
  modules: {
  }
})
