import axios from 'axios';


/**
 * Класс для работы c API
 */
class WebClient {
  /**
   * Конструктор класс
   * @param {String} baseURL Базовый url бекэнда
   */
  constructor(baseURL) {
    this.baseURL = baseURL;

    this.store = null;
    this.router = null;

    this.axios = axios.create({
      baseURL,
      headers: {
				'Content-Type': 'application/json',
      },
      withCredentials: true,
    });

    this.setInterceptors();
  }

  /**
   * Установка обработчиков 
   */
  setInterceptors() {
    this.axios.interceptors.response.use(res => {
			/// Постобработка усппешных запросов
			return res;
    }, error => {
      /// Постобработка не усппешных запросов

      // if ([401, 403].includes(error.data.status)) {
      //   this.store.dispatch('unsetUserData');
      //   setTimeout(() => {
      //     this.router.replace({ name: "login" });
      //   }, 0);
			// }
			
      return error;
    });
	}
  /**
	 * Метод установки заголовка авторизации после входа пользователем в приложение
	 * @param {String} token Токен авторизации
   */
	login(token) {
		this.axios.defaults.baseURL = this.baseURL + 'api/';
		this.axios.defaults.headers.common['authorization'] = 'Bearer ' + token;
  }
  /**
   * Метод очистки заголовка авторизации при выходе пользователя из приложения
   */
  logout() {
		this.axios.defaults.baseURL = this.baseURL;
    delete this.axios.defaults.headers.common['authorization'];
  }

  get(url, params, config) {
    return this.axios.get(url, {
      params, ...config
    })
  }
  post(url, data, config) {
    return this.axios.post(url, data, config)
  }
  put(url, data, config) {
    return this.axios.put(url, data, config)
  }
  delete(url, config) {
    return this.axios.delete(url, config)
  }
  request(url, config) {
    return this.axios.request({
      url,
      ...config
    });
  }
};

/**
 * URL API
 */
const url = 'https://localhost:44343/';
// const url = 'https://event-band-api.ru:5000/';
export default new WebClient(url);

