import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "about" */ '../views/Home.vue')
  },
  {
    path: '/userA',
    name: 'UserAdmin',
    component: () => import(/* webpackChunkName: "about" */ '../views/UserAdmin.vue')
  },
  {
    path: '/orderA',
    name: 'OrderAdmin',
    component: () => import(/* webpackChunkName: "about" */ '../views/OrderAdmin.vue')
  },
  {
    path: '/orderU',
    name: 'OrderUser',
    component: () => import(/* webpackChunkName: "about" */ '../views/OrderUser.vue')
  },
  {
    path: '/cart',
    name: 'Cart',
    component: () => import(/* webpackChunkName: "about" */ '../views/Cart.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
